/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.requestlog.itest;

import org.amdatu.mongo.MongoDBService;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

public class MongoTestUtil {

    /**
     * Provides a way to check if the requested collection is available.
     * The Database always returns a collection regardless if an actual mongoDB instance is listening,
     * this method provides an extra check to guarantee availability at the point of calling.
     * 
     * @param service The MongoDBService for which the collection should be retrieved
     * @param collection The name of the collection to be retrieved
     * @return The requested collection or null if unavailable
     * @throws Exception
     */
    public static DBCollection getDBCollection(MongoDBService service, String collection) throws Exception {
        try {
            DBCollection col = service.getDB().getCollection(collection);
            // we always get a collection back, regardless if there is an actual MongoDB listening, hence we should do
            // some actual calls that cause a connection to MongoDB to be created...
            if (col.getCount() > 0L) {
                col.remove(new BasicDBObject());
            }
            return col;
        } catch (Exception exception) {
            System.err.println("Mongodb not available on localhost, skipping test...");
            return null;
        }
    }

}