/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.requestlog.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.testing.mongo.MongoConfigurationStep;
import org.amdatu.testing.mongo.OSGiMongoTestConfigurator;
import org.amdatu.web.requestlog.api.LogFilter;
import org.amdatu.web.requestlog.api.RequestLog;
import org.amdatu.web.requestlog.api.RequestLogStore;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;

import com.mongodb.DBCollection;

public class RequestLogStoreTest extends TestCase {

    private volatile MongoDBService m_mongoDBService;
    private volatile RequestLogStore m_requestLogStore;
    private static final String COLLECTION = "requestlog";
    private DBCollection m_collection;
    private final BundleContext context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
    private MongoConfigurationStep m_mongoConfiguration;

    @Override
    public void setUp() throws Exception {
        // The bundle might have been stopped by the tearDown method.
        startBundle("org.amdatu.web.requestlog.mongo");
        
        //manually create config step and apply'ing it
        m_mongoConfiguration = OSGiMongoTestConfigurator.configureMongoDb();
        m_mongoConfiguration.apply(this);
        
        configure(this)
            .add(createServiceDependency().setService(RequestLogStore.class).setRequired(true)) // inject RequestLogService
            .add(createServiceDependency().setService(MongoDBService.class).setRequired(true)) // inject the mongoDBService
            .add(createConfiguration("org.amdatu.web.requestlog.mongo").set("timerInterval", "10"))
            .apply();

        super.setUp();
        
        // Check if the configured collection is actually available
        m_collection = MongoTestUtil.getDBCollection(m_mongoDBService, COLLECTION);
    }
    
    @Override
    protected void tearDown() throws Exception {
        if (isMongoAvailable()) {
            // Dropping the collection while flush is still running potentially results in an exception, so stopping the bundle first.
            stopBundle("org.amdatu.web.requestlog.mongo");
            m_mongoConfiguration.cleanUp(this);
        }
        super.tearDown();        
        cleanUp(this);
    }

    public void testAggregateHits() throws InterruptedException {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();

        TimeUnit.MILLISECONDS.sleep(100);
        Map<String, Integer> results = m_requestLogStore.aggregateHits(LogFilter.createDefault());

        assertEquals(3, results.size());
        assertEquals(3, results.get("/test"), 0.1);
        assertEquals(2, results.get("/other"), 0.1);
        assertEquals(2, results.get("/andanother"), 0.1);
    }

    public void testAggregateHitsLimit() throws InterruptedException {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();

        TimeUnit.MILLISECONDS.sleep(100);
        Map<String, Integer> results = m_requestLogStore.aggregateHits(LogFilter.create().nrOfResults(1).build());

        assertEquals(1, results.size());
        assertEquals(3, results.get("/test"), 0.1);
    }

    public void testAggregateHitsMinimumResponseTime() throws InterruptedException {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();

        TimeUnit.MILLISECONDS.sleep(100);
        Map<String, Integer> results =
            m_requestLogStore.aggregateHits(LogFilter.create().minimumResponseTime(100).build());

        assertEquals(2, results.size());
        assertEquals(1, results.get("/test"), 0.1);
        assertEquals(2, results.get("/other"), 0.1);
    }

    public void testAverageResponseTime() throws InterruptedException {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();

        TimeUnit.MILLISECONDS.sleep(100);
        Map<String, Double> results = m_requestLogStore.aggregateAverageResponseTime(LogFilter.createDefault());

        assertEquals(3, results.size());
        assertEquals(75, results.get("/test"), 0.1);
        assertEquals(150, results.get("/other"), 0.1);
        assertEquals(15, results.get("/andanother"), 0.1);
    }

    public void testAverageResponseTimeFilterDate() throws InterruptedException {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();
        m_requestLogStore.storeLog(new RequestLog("/test", 10, "chrome", 10000));
        m_requestLogStore.storeLog(new RequestLog("/test", System.currentTimeMillis() - 20000, "chrome", 100));
        m_requestLogStore.storeLog(new RequestLog("/oldevent", System.currentTimeMillis() - 30000, "chrome", 75));

        TimeUnit.MILLISECONDS.sleep(100);
        Map<String, Double> results =
            m_requestLogStore.aggregateAverageResponseTime(LogFilter.create()
                .dateLowerBound(System.currentTimeMillis() - 40000).dateUpperBound(System.currentTimeMillis() - 5000)
                .build());

        assertEquals(2, results.size());
        assertEquals(100, results.get("/test"), 0.1);
        assertEquals(75, results.get("/oldevent"), 0.1);
    }

    public void testAverageResponseTimeFilterMinimumResponseTime() throws InterruptedException {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();

        TimeUnit.MILLISECONDS.sleep(100);
        Map<String, Double> results =
            m_requestLogStore.aggregateAverageResponseTime(LogFilter.create().minimumResponseTime(100).build());

        assertEquals(1, results.size());
        assertEquals(150, results.get("/other"), 0.1);
    }

    public void testAverageResponseTimeLimitResults() throws InterruptedException {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();

        TimeUnit.MILLISECONDS.sleep(100);
        Map<String, Double> results =
            m_requestLogStore.aggregateAverageResponseTime(LogFilter.create().nrOfResults(2).build());

        assertEquals(2, results.size());
        assertEquals(75, results.get("/test"), 0.1);
        assertEquals(150, results.get("/other"), 0.1);
    }

    public void testFindWithDateFilter() throws Exception {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();
        m_requestLogStore.storeLog(new RequestLog("/old", 10, "chrome", 50));
        m_requestLogStore.storeLog(new RequestLog("/shouldbefound", System.currentTimeMillis() - 20000, "chrome", 50));

        TimeUnit.MILLISECONDS.sleep(100);
        List<RequestLog> list =
            m_requestLogStore.list(LogFilter.create().dateLowerBound(System.currentTimeMillis() - 30000)
                .dateUpperBound(System.currentTimeMillis() - 10000).build());
        assertEquals(1, list.size());
    }

    public void testFindWithMinimumResponseTime() throws Exception {
        if (!isMongoAvailable()) {
            return;
        }
        createTestData();
        TimeUnit.MILLISECONDS.sleep(100);
        List<RequestLog> list = m_requestLogStore.list(LogFilter.create().minimumResponseTime(100).build());
        assertEquals(3, list.size());
    }

    public void testStoreAndFind() throws Exception {
        if (!isMongoAvailable()) {
            return;
        }
        m_requestLogStore.storeLog(new RequestLog("/test", System.currentTimeMillis(), "chrome", 200));
        TimeUnit.MILLISECONDS.sleep(100);
        List<RequestLog> list = m_requestLogStore.list(LogFilter.createDefault());
        assertEquals(1, list.size());
    }

    private void createTestData() {
        m_requestLogStore.storeLog(new RequestLog("/test", System.currentTimeMillis(), "chrome", 50));
        m_requestLogStore.storeLog(new RequestLog("/test", System.currentTimeMillis(), "chrome", 75));
        m_requestLogStore.storeLog(new RequestLog("/test", System.currentTimeMillis(), "chrome", 100));

        m_requestLogStore.storeLog(new RequestLog("/other", System.currentTimeMillis(), "chrome", 200));
        m_requestLogStore.storeLog(new RequestLog("/other", System.currentTimeMillis(), "chrome", 100));

        m_requestLogStore.storeLog(new RequestLog("/andanother", System.currentTimeMillis(), "chrome", 10));
        m_requestLogStore.storeLog(new RequestLog("/andanother", System.currentTimeMillis(), "chrome", 20));
    }

    private boolean isMongoAvailable() {
        return m_collection != null;
    }

    private void startBundle(String bundleSymbolicName) {
        for (Bundle bundle : context.getBundles()) {
            if (bundle.getSymbolicName().equals(bundleSymbolicName)) {
                try {
                    bundle.start();
                } catch (BundleException e) {
                    throw new RuntimeException(e);
                }
                return;
            }
        }
    }

    private void stopBundle(String bundleSymbolicName) {
        for (Bundle bundle : context.getBundles()) {
            if (bundle.getSymbolicName().equals(bundleSymbolicName)) {
                try {
                    bundle.stop();
                } catch (BundleException e) {
                    throw new RuntimeException(e);
                }
                return;
            }
        }
    }

}