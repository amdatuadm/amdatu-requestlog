/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.requestlog.mongo;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.web.requestlog.api.LogFilter;
import org.amdatu.web.requestlog.api.RequestLog;
import org.amdatu.web.requestlog.api.RequestLogStore;
import org.mongojack.DBCursor;
import org.mongojack.JacksonDBCollection;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class MongoRequestLogStore implements RequestLogStore, ManagedService {
    private volatile String m_collectionName = "requestlog";
    private volatile long m_collectionSize = 1000000;
    private volatile long m_timerInterval = 10000L;
    
    private final LinkedBlockingQueue<RequestLog> m_logbuffer = new LinkedBlockingQueue<>();
	private volatile MongoDBService m_mongoDBService;
	private volatile DBCollection m_collection;
	private volatile JacksonDBCollection<RequestLog, String> m_logs;
	private final Timer m_timer = new Timer();
	private volatile TimerTask m_timerTask;    

	public synchronized void start() {
	    setUpCollection();
	    
	    m_collection = m_mongoDBService.getDB().getCollection(m_collectionName);
        m_logs = JacksonDBCollection.wrap(m_collection, RequestLog.class, String.class);
        
        rescheduleTimerTask();
	}
	
	public synchronized void stop() {
	    m_timer.cancel();
	    m_collection = null;
	    m_logs = null;
	}
	
	/**
     * Preparing the capped collection.
     */
    private void setUpCollection() {
        if (m_mongoDBService == null) {
            return;
        }
        if (!m_mongoDBService.getDB().collectionExists(m_collectionName)) {
                DBObject options = BasicDBObjectBuilder.start().add("capped", true).add("size", m_collectionSize).get();
                m_mongoDBService.getDB().createCollection(m_collectionName, options);
        }
    }
	
	@Override
	public List<RequestLog> list(LogFilter filter) {
		
	    if(filter == null) {
	        filter = LogFilter.createDefault();
	    }
	    
        List<RequestLog> result = new ArrayList<>();
        
        BasicDBObjectBuilder queryBuilder = new BasicDBObjectBuilder();
        
        if(filter.getMinimumResponseTime() > 0) {
            queryBuilder.add("responseTime", new BasicDBObject("$gte", filter.getMinimumResponseTime()));
        }
        
        createDateFilter(filter, queryBuilder);
        
        DBCursor<RequestLog> cursor = m_logs.find(queryBuilder.get());
        cursor.sort(new BasicDBObject("timestamp", -1));
        
        if(filter.getNrOfResults() > 0) {
            cursor.limit(filter.getNrOfResults());
        }
        
        while(cursor.hasNext()) {
        	result.add(cursor.next());
        }
        	
        return result;
	}

    @Override
    public void storeLog(RequestLog requestLog) {
        setUpCollection();
        
        try {
            m_logbuffer.put(requestLog);
        }
        catch (InterruptedException e) {
            //The log entry will be lost, we will not try to recover this.
        }
    }
    
    private synchronized void flush() {
        
        if(m_collection != null && m_logs != null) {
            setUpCollection();
            List<RequestLog> logsToFlush = new ArrayList<>();
            m_logbuffer.drainTo(logsToFlush);
            
            if (!logsToFlush.isEmpty()) {
                m_logs.insert(logsToFlush);
            }
        }
    }
    
    private void rescheduleTimerTask() {
        if (m_timerTask != null) {
            m_timerTask.cancel();
        }
        m_timerTask = new TimerTask() {

            @Override
            public void run() {
                flush();
            }
        };
        m_timer.scheduleAtFixedRate(m_timerTask, m_timerInterval, m_timerInterval);

    }

    @Override
    public Map<String, Integer> aggregateHits(LogFilter filter) {
        if(filter == null) {
            filter = LogFilter.createDefault();
        }
        
        BasicDBObjectBuilder matchBuilder = new BasicDBObjectBuilder();
        
        if(filter.getMinimumResponseTime() > 0) {
            matchBuilder.add("responseTime", new BasicDBObject("$gte", filter.getMinimumResponseTime()));
        }
        
        createDateFilter(filter, matchBuilder);
        
        DBObject match = new BasicDBObject("$match", matchBuilder.get());
        DBObject group = new BasicDBObject("$group", new BasicDBObjectBuilder().add("_id", "$path").add("nrOfRequests", new BasicDBObject("$sum", 1)).get());
        DBObject sort = new BasicDBObject("$sort", new BasicDBObject("nrOfRequests", -1));
        DBObject limit = new BasicDBObject("$limit", filter.getNrOfResults() > 0 ? filter.getNrOfResults() : 100000);
        
        Iterable<DBObject> results = m_collection.aggregate(match, group, sort, limit).results();
        Iterator<DBObject> iterator = results.iterator();
        
        Map<String, Integer> result = new HashMap<>();
        while(iterator.hasNext()) {
            DBObject next = iterator.next();
            result.put((String)next.get("_id"), (Integer)next.get("nrOfRequests"));
        }
        
        return result;
    }

    private void createDateFilter(LogFilter filter, BasicDBObjectBuilder matchBuilder) {
        if(filter.getDateUpperBound() > 0) {
            BasicDBList andOps = new BasicDBList();
            andOps.add(new BasicDBObject("timestamp", new BasicDBObject("$lte", filter.getDateUpperBound())));
            andOps.add(new BasicDBObject("timestamp", new BasicDBObject("$gte", filter.getDateLowerBound())));
            matchBuilder.add("$and", andOps);
        } else {
            matchBuilder.add("timestamp", new BasicDBObject("$gte", filter.getDateLowerBound()));
        }
    }

    @Override
    public Map<String, Double> aggregateAverageResponseTime(LogFilter filter) {
        if(filter == null) {
            filter = LogFilter.createDefault();
        }
        
        BasicDBObjectBuilder matchBuilder = new BasicDBObjectBuilder();
        
        matchBuilder.add("timestamp", new BasicDBObject("$gt", filter.getDateLowerBound()));
        
        createDateFilter(filter, matchBuilder);
        
        DBObject match = new BasicDBObject("$match", matchBuilder.get());
        DBObject group = new BasicDBObject("$group", new BasicDBObjectBuilder().add("_id", "$path").add("responseTime", new BasicDBObject("$avg", "$responseTime")).get());
        DBObject postAvgMatch = new BasicDBObject("$match", new BasicDBObject("responseTime", new BasicDBObject("$gt", filter.getMinimumResponseTime())));
        DBObject sort = new BasicDBObject("$sort", new BasicDBObject("responseTime", -1));
        DBObject limit = new BasicDBObject("$limit", filter.getNrOfResults() > 0 ? filter.getNrOfResults() : 100000);
        
        Iterable<DBObject> results = m_collection.aggregate(match, group, postAvgMatch, sort, limit).results();
        Iterator<DBObject> iterator = results.iterator();
        
        Map<String, Double> result = new HashMap<>();
        while(iterator.hasNext()) {
            DBObject next = iterator.next();
            result.put((String)next.get("_id"), (Double)next.get("responseTime"));
        }
        
        return result;
    }

    @Override
    public void updated(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {
    	if(properties == null) {
    		return;
    	}
    	
        setCollectionNameFromConfig(properties);
        setCollectionSizeFromConfig(properties);
        setTimerIntervalFromConfig(properties);
        
        rescheduleTimerTask();
    }

    private void setCollectionNameFromConfig(@SuppressWarnings("rawtypes") Dictionary properties) {
    	String collectionName = (String)properties.get("collectionName");
        
        if(collectionName != null) {
            m_collectionName = collectionName;
        }
    }
    
    private void setCollectionSizeFromConfig(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {
        String collectionSizeStr = (String)properties.get("collectionSize");
        
        if(collectionSizeStr != null) {
            try {
                m_collectionSize = Long.parseLong(collectionSizeStr);
            } catch(NumberFormatException ex) {
                throw new ConfigurationException("collectionSize", "collectionSize is not a number");
            }
        }
    }
    
    private void setTimerIntervalFromConfig(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {
        String timerIntervalStr = (String)properties.get("timerInterval");
        
        if(timerIntervalStr != null) {
            try {
                m_timerInterval = Long.parseLong(timerIntervalStr);
            } catch(NumberFormatException ex) {
                throw new ConfigurationException("timerInterval", "timerInterval is not a number");
            }
        }
    }
}
