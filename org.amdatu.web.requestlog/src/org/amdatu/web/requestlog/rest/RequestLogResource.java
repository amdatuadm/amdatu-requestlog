/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.requestlog.rest;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.amdatu.web.requestlog.api.LogFilter;
import org.amdatu.web.requestlog.api.RequestLog;
import org.amdatu.web.requestlog.api.RequestLogStore;

@Path("requestlog")
public class RequestLogResource {
    private volatile RequestLogStore m_logStore;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<RequestLog> list(@QueryParam("nrOfResults") int nrOfResults,
        @QueryParam("minimumResponseTime") long minimumResponseTime, @QueryParam("dateLowerBound") long dateLowerBound,
        @QueryParam("dateUpperBound") long dateUpperBound) {

        LogFilter filter =
            LogFilter.create().nrOfResults(nrOfResults).minimumResponseTime(minimumResponseTime)
                .dateLowerBound(dateLowerBound).dateUpperBound(dateUpperBound).build();

        return m_logStore.list(filter);
    }
    
    @Path("responsetimes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Double> listAvgResponseTimes(@QueryParam("nrOfResults") int nrOfResults,
        @QueryParam("minimumResponseTime") long minimumResponseTime, @QueryParam("dateLowerBound") long dateLowerBound,
        @QueryParam("dateUpperBound") long dateUpperBound) {

        LogFilter filter =
            LogFilter.create().nrOfResults(nrOfResults).minimumResponseTime(minimumResponseTime)
                .dateLowerBound(dateLowerBound).dateUpperBound(dateUpperBound).build();

        return m_logStore.aggregateAverageResponseTime(filter);
    }
    
    @Path("hits")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Integer> listHits(@QueryParam("nrOfResults") int nrOfResults,
        @QueryParam("minimumResponseTime") long minimumResponseTime, @QueryParam("dateLowerBound") long dateLowerBound,
        @QueryParam("dateUpperBound") long dateUpperBound) {

        LogFilter filter =
            LogFilter.create().nrOfResults(nrOfResults).minimumResponseTime(minimumResponseTime)
                .dateLowerBound(dateLowerBound).dateUpperBound(dateUpperBound).build();

        return m_logStore.aggregateHits(filter);
    }
}
