/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.requestlog.webviewer;

import java.util.Properties;

import javax.servlet.Servlet;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
	@Override
	public void init(BundleContext ctx, DependencyManager dm) throws Exception {

		Properties props = new Properties();
		props.put("alias", "/requestlogviewer/config");
		props.put(Constants.SERVICE_PID, "org.amdatu.web.requestlog.webviewer");

		dm.add(createComponent().setInterface(new String[] { Servlet.class.getName(), ManagedService.class.getName()}, props)
				.setImplementation(WebviewerConfigServlet.class)
				.add(createServiceDependency().setService(LogService.class)));
	}

	@Override
	public void destroy(BundleContext ctx, DependencyManager dm) throws Exception {
	}

}
