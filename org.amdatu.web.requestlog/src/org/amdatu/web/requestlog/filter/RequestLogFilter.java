/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.requestlog.filter;

import java.io.IOException;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.amdatu.web.requestlog.api.RequestLog;
import org.amdatu.web.requestlog.api.RequestLogStore;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

public class RequestLogFilter implements Filter, ManagedService {
    private volatile RequestLogStore m_logStore;
    private final Set<String> m_pathsToFilter = new HashSet<>();
    
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        long startTime = System.currentTimeMillis();
        chain.doFilter(servletRequest, servletResponse);
        long spendTime = System.currentTimeMillis() - startTime;
  
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        
        if(m_pathsToFilter.size() == 0 || matchesPathsToFilter(request.getRequestURI())) {
            m_logStore.storeLog(new RequestLog(request.getMethod() + " " + request.getRequestURI(), startTime, request.getHeader("User-Agent"), spendTime));
        }
    }

    private boolean matchesPathsToFilter(String requestURI) {
        for (String expression : m_pathsToFilter) {
            if(requestURI.matches(expression)) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
    }
    
    @Override
    public void destroy() {
    }

    @Override
    public synchronized void updated(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {
        if(properties != null) {
        	String pathsToFilterConfig = (String)properties.get("pathsToFilter");
            if(pathsToFilterConfig != null) {
                 String[] split = pathsToFilterConfig.replaceAll(" ", "").split(",");
                 m_pathsToFilter.clear();
                 
                 for (String path : split) {
                    m_pathsToFilter.add(path);
                }
            }	
        }
    }

}
