/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.requestlog.api;

import java.util.List;
import java.util.Map;

public interface RequestLogStore {
    /**
     * @return Lists log entries, newest entries are returned first.
     * @param filter may be null
     */
    List<RequestLog> list(LogFilter filter);

    /**
     * Store a new log entry
     */
    void storeLog(RequestLog requestLog);

    /**
     *  
     * @param filter may be null
     * @return Log entries aggregated by path, showing the number of hits.
     */
    Map<String, Integer> aggregateHits(LogFilter filter);
    
    /**
     * @param filter may be null
     * @return Log entries aggregated by path, showing the average response time on each path.
     */
    Map<String, Double> aggregateAverageResponseTime(LogFilter filter);
}