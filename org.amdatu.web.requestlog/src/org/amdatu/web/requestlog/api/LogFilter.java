/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.requestlog.api;

public class LogFilter {
    private final int m_nrOfResults;
    private final long m_minimumResponseTime;
    private final long m_dateLowerBound;
    private final long m_dateUpperBound;

    private LogFilter(int nrOfResults, long minimumResponseTime, long dateLowerBound, long dateUpperBound) {
        m_nrOfResults = nrOfResults;
        m_minimumResponseTime = minimumResponseTime;
        m_dateLowerBound = dateLowerBound;
        m_dateUpperBound = dateUpperBound;
    }

    public int getNrOfResults() {
        return m_nrOfResults;
    }

    public long getMinimumResponseTime() {
        return m_minimumResponseTime;
    }

    public long getDateLowerBound() {
        return m_dateLowerBound;
    }

    public long getDateUpperBound() {
        return m_dateUpperBound;
    }

    @Override
    public String toString() {
        return "LogFilter [m_nrOfResults=" + m_nrOfResults + ", m_minimumTimeSpent=" + m_minimumResponseTime
            + ", m_dateLowerBound=" + m_dateLowerBound + ", m_dateUpperBound=" + m_dateUpperBound + "]";
    }
    
    public static Builder create() {
        return new Builder();
    }
    
    public static LogFilter createDefault() {
        return new LogFilter(0, 0, 0, 0);
    }

    public static class Builder {
        private int m_nrOfResults;
        private long m_minimumResponseTime;
        private long m_dateLowerBound;
        private long m_dateUpperBound;
        
        public Builder nrOfResults(int nrOfResults) {
            m_nrOfResults = nrOfResults;
            return this;
        }
        
        public Builder minimumResponseTime(long minimumResponseTime) {
            m_minimumResponseTime = minimumResponseTime;
            return this;
        }
        
        public Builder dateLowerBound(long dateLowerBound) {
            m_dateLowerBound = dateLowerBound;
            return this;
        }
        
        public Builder dateUpperBound(long dateUpperBound) {
            m_dateUpperBound = dateUpperBound;
            return this;
        }
        
        public LogFilter build() {
            return new LogFilter(m_nrOfResults, m_minimumResponseTime, m_dateLowerBound, m_dateUpperBound);
        }
    }
}
