'use strict';

var ctrlModule = angular.module('logviewer.controllers', []);    
ctrlModule.controller('HitsCtrl', ['$scope', '$http', '$location', 'configuration', function ($scope, $http, $location, configuration){
	
	$scope.$watch('restPath', function() {
		if($scope.restPath != undefined) {
			$scope.load();
		}
	}); 
	
	$scope.nrOfResults = 100;
	if($location.path() == '/hits') {
		$scope.type = 'hits';
	} else {
		$scope.type = 'responsetimes';
	}	
		
	$scope.load = function() {
	
		$http.get($scope.restPath + "/requestlog/" + $scope.type + "?nrOfResults=" + $scope.nrOfResults).success(function(data) {
			
			var tuples = [];
			angular.forEach(data, function(val, key) {
				tuples.push([key, val]);
			});
			
			tuples.sort(function(a,b) {
				return a[1] > b[1] ? -1 : (a[1] < b[1] ? 1 : 0);
			});
			
			$scope.data = tuples;
		});	
	}
	
	
	
}]);
	
ctrlModule.controller('LogsCtrl', ['$scope', '$http', '$location', 'configuration', function ($scope, $http, $location, configuration){
	$scope.nrOfResults = 100;
	
	$scope.$watch('restPath', function() {
		if($scope.restPath != undefined) {
			$scope.load();
		}
	});
			
	$scope.load = function() {
		$http.get($scope.restPath + "/requestlog?nrOfResults=" + $scope.nrOfResults).success(function(data) {
			$scope.data = data;			
		});	
	}
}]);
