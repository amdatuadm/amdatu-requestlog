'use strict';
angular.module('logviewer', ['logviewer.controllers', 'ngRoute']).
	config(['$routeProvider', function($routeProvider) {
		$routeProvider.
		when('/hits', {
			templateUrl : 'partials/hits.html',
			controller : 'HitsCtrl',
			reloadOnSearch: false,
		}).
		when('/responsetimes', {
			templateUrl : 'partials/responsetimes.html',
			controller : 'HitsCtrl',
			reloadOnSearch: false,
		}).
		when('/logs', {
			templateUrl : 'partials/logs.html',
			controller : 'LogsCtrl',
			reloadOnSearch: false,
		}).
		otherwise( { redirectTo : '/hits' } );
}]).directive('navbar', function($location) {
	
    return {
      templateUrl: 'partials/navbar.html',
      restrict: 'E',
      link: function(scope, element, attrs) {
      	function setActive() {
	      	$("li").removeClass("active");
	      	
	      	var classToFind = $location.path().substr(1);
	      	$("." + classToFind).addClass("active");
	    } 	
	    
	    setActive();  
	      
		scope.$watch(function() {return $location.path()}, function(path) {
			setActive();
		});
      }	
      	
    };
  })
.factory('configuration',['$http', '$rootScope', function($http, $rootScope) {
  $http.get("/requestlogviewer/config").success(function(data) {
  	$rootScope.restPath = data;
  });   
}]);
